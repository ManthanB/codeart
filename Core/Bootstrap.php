<?php
namespace Framework\Core;

class Bootstrap {

	public function __construct(){
		if(isset($_GET['url']) && $_GET['url'] != ''){
			$url = explode("/",  rtrim($_GET['url'],"/"));
			$app = (isset($url[0])) ? $url[0] : 'frontend';
			$controller = (isset($url[1])) ? $url[1] : null;
			$action = (isset($url[2])) ? $url[2] : null;
			// print_r($_GET);
			$params = array();
			if(count($url) > 3){
				foreach($url as $key => $param){
					if($key > 2){
						$params[] = $param;
					}
				}
			}
		}else{
			$app = 'frontend';
			$controller = null;
			$action = null;
			$params = array();
		}
		$this->render($app, $controller, $action, $params);
	}

	/**
	 * 
	 * @param  string $app
	 * @param  string $controller
	 * @param  string $action
	 * @param  array  $params
	 * @return 
	 */
	function render($app, $controller, $action, $params = array()){
		if($controller == null){
			$config = require __DIR__.'/../Apps/'.ucfirst($app).'/config.php';
			if(isset($config['controllers']) && count($config['controllers']) > 0){
				$controller = array_keys(reset($config))[0];
				$action = $config['controllers'][$controller][0];
			}
		}
		$callableControllerName = "Apps\\".ucfirst($app)."\\Controllers\\".ucfirst($controller)."Controller";
		$callableController = new $callableControllerName($app);
		$callableController->injectRepositories();
		call_user_func_array(array($callableController, $action."Action"), $params);
	}
}
?>