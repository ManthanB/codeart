<?php
namespace Framework\Core\Frontend;

abstract class AbstractController {
	function __construct($app){
		$this->app = $app;
		
		$this->view = new \Framework\Core\Frontend\View();

		$paths = $this->view->getTemplatePaths();
		$root = __DIR__."/../../";
		$paths->setTemplateRootPaths([
    			$root."/Apps/".ucfirst($app)."/Resources/Private/Templates/"
			]);
			$paths->setLayoutRootPaths([
			    $root."/Apps/".ucfirst($app)."/Resources/Private/Layouts/"
			]);
			$paths->setPartialRootPaths([
			    $root."/Apps/".ucfirst($app)."/Resources/Private/Partials/"
			]);
	}

	public function injectRepository($repository){
		return new $repository($this->app);
	}

	public function render(){
		$this->e = new \Exception();
	    $trace = $this->e->getTrace();
	    //position 0 would be the line that called this function so we ignore it
	    $last_call = $trace[1];
		$func = $last_call['function'];
		$view = substr($func, 0, strpos($func, 'Action'));
		$controller = explode("\\", get_called_class());

		$controllerName = $controller[count($controller)-1];
		$finalController = substr($controllerName, 0, strpos($controllerName,'Controller'));
		
		$this->view->getRenderingContext()->setControllerName($finalController);
		$this->view->getRenderingContext()->setControllerAction($view);
		echo $this->view->render();
	}
}