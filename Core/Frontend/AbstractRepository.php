<?php
namespace Framework\Core\Frontend;

abstract class AbstractRepository {

	protected $table;

	protected $db;

	function __construct($app){
		$className = get_called_class();
		$list = substr($className, strpos($className, "Repository"));
		$table = explode("\\", $list)[1];
		$tableName = substr($table, 0, strpos($table, 'Repository'));
		$this->table = strtolower($tableName);
		
		$dbconfig = require __DIR__."/DatabaseConfig.php";
		$this->db = new \MySQLi($dbconfig['host'], $dbconfig['user'], $dbconfig['pass'], $dbconfig['database']);
		// print_r($this->db);
		// echo $this->table;
	}

	public function findAll(){
		$sql = "SELECT * FROM $table ";

		$this->execute($sql);
	}

	public function execute($sql){
		$resource = $this->db->query($sql);

		if(is_array($resource)){
			$rows = array();
			while($row = $this->db->fetch_assoc($resource)){
				$rows[] = $row;
			}
			return $rows;
		}else{
			return $resource;
		}
	}

}

?>