<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Apps\\Frontend\\Controllers\\DashboardController' => $baseDir . '/Apps/Frontend/Classes/Controllers/DashboardController.php',
    'Apps\\Frontend\\Repository\\DashboardRepository' => $baseDir . '/Apps/Frontend/Classes/Repository/DashboardRepository.php',
    'Framework\\Core\\Bootstrap' => $baseDir . '/Core/Bootstrap.php',
    'Framework\\Core\\Frontend\\AbstractController' => $baseDir . '/Core/Frontend/AbstractController.php',
    'Framework\\Core\\Frontend\\AbstractRepository' => $baseDir . '/Core/Frontend/AbstractRepository.php',
    'Framework\\Core\\Frontend\\AbstractView' => $baseDir . '/Core/Frontend/AbstractView.php',
    'Framework\\Core\\Frontend\\View' => $baseDir . '/Core/Frontend/View.php',
    'TYPO3Fluid\\Fluid\\Core\\Cache\\FluidCacheInterface' => $baseDir . '/Core/Fluid/src/Core/Cache/FluidCacheInterface.php',
    'TYPO3Fluid\\Fluid\\Core\\Cache\\FluidCacheWarmerInterface' => $baseDir . '/Core/Fluid/src/Core/Cache/FluidCacheWarmerInterface.php',
    'TYPO3Fluid\\Fluid\\Core\\Cache\\FluidCacheWarmupResult' => $baseDir . '/Core/Fluid/src/Core/Cache/FluidCacheWarmupResult.php',
    'TYPO3Fluid\\Fluid\\Core\\Cache\\SimpleFileCache' => $baseDir . '/Core/Fluid/src/Core/Cache/SimpleFileCache.php',
    'TYPO3Fluid\\Fluid\\Core\\Cache\\StandardCacheWarmer' => $baseDir . '/Core/Fluid/src/Core/Cache/StandardCacheWarmer.php',
    'TYPO3Fluid\\Fluid\\Core\\Compiler\\AbstractCompiledTemplate' => $baseDir . '/Core/Fluid/src/Core/Compiler/AbstractCompiledTemplate.php',
    'TYPO3Fluid\\Fluid\\Core\\Compiler\\FailedCompilingState' => $baseDir . '/Core/Fluid/src/Core/Compiler/FailedCompilingState.php',
    'TYPO3Fluid\\Fluid\\Core\\Compiler\\NodeConverter' => $baseDir . '/Core/Fluid/src/Core/Compiler/NodeConverter.php',
    'TYPO3Fluid\\Fluid\\Core\\Compiler\\StopCompilingChildrenException' => $baseDir . '/Core/Fluid/src/Core/Compiler/StopCompilingChildrenException.php',
    'TYPO3Fluid\\Fluid\\Core\\Compiler\\StopCompilingException' => $baseDir . '/Core/Fluid/src/Core/Compiler/StopCompilingException.php',
    'TYPO3Fluid\\Fluid\\Core\\Compiler\\TemplateCompiler' => $baseDir . '/Core/Fluid/src/Core/Compiler/TemplateCompiler.php',
    'TYPO3Fluid\\Fluid\\Core\\Compiler\\ViewHelperCompiler' => $baseDir . '/Core/Fluid/src/Core/Compiler/ViewHelperCompiler.php',
    'TYPO3Fluid\\Fluid\\Core\\Exception' => $baseDir . '/Core/Fluid/src/Core/Exception.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\BooleanParser' => $baseDir . '/Core/Fluid/src/Core/Parser/BooleanParser.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\Configuration' => $baseDir . '/Core/Fluid/src/Core/Parser/Configuration.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\Exception' => $baseDir . '/Core/Fluid/src/Core/Parser/Exception.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\InterceptorInterface' => $baseDir . '/Core/Fluid/src/Core/Parser/InterceptorInterface.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\Interceptor\\Escape' => $baseDir . '/Core/Fluid/src/Core/Parser/Interceptor/Escape.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\ParsedTemplateInterface' => $baseDir . '/Core/Fluid/src/Core/Parser/ParsedTemplateInterface.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\ParsingState' => $baseDir . '/Core/Fluid/src/Core/Parser/ParsingState.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\PassthroughSourceException' => $baseDir . '/Core/Fluid/src/Core/Parser/PassthroughSourceException.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\Patterns' => $baseDir . '/Core/Fluid/src/Core/Parser/Patterns.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\AbstractNode' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/AbstractNode.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\ArrayNode' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/ArrayNode.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\BooleanNode' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/BooleanNode.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\EscapingNode' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/EscapingNode.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\Expression\\AbstractExpressionNode' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/Expression/AbstractExpressionNode.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\Expression\\CastingExpressionNode' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/Expression/CastingExpressionNode.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\Expression\\ExpressionException' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/Expression/ExpressionException.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\Expression\\ExpressionNodeInterface' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/Expression/ExpressionNodeInterface.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\Expression\\MathExpressionNode' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/Expression/MathExpressionNode.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\Expression\\TernaryExpressionNode' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/Expression/TernaryExpressionNode.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\NodeInterface' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/NodeInterface.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\NumericNode' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/NumericNode.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\ObjectAccessorNode' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/ObjectAccessorNode.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\RootNode' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/RootNode.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\TextNode' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/TextNode.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\SyntaxTree\\ViewHelperNode' => $baseDir . '/Core/Fluid/src/Core/Parser/SyntaxTree/ViewHelperNode.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\TemplateParser' => $baseDir . '/Core/Fluid/src/Core/Parser/TemplateParser.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\TemplateProcessorInterface' => $baseDir . '/Core/Fluid/src/Core/Parser/TemplateProcessorInterface.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\TemplateProcessor\\EscapingModifierTemplateProcessor' => $baseDir . '/Core/Fluid/src/Core/Parser/TemplateProcessor/EscapingModifierTemplateProcessor.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\TemplateProcessor\\NamespaceDetectionTemplateProcessor' => $baseDir . '/Core/Fluid/src/Core/Parser/TemplateProcessor/NamespaceDetectionTemplateProcessor.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\TemplateProcessor\\PassthroughSourceModifierTemplateProcessor' => $baseDir . '/Core/Fluid/src/Core/Parser/TemplateProcessor/PassthroughSourceModifierTemplateProcessor.php',
    'TYPO3Fluid\\Fluid\\Core\\Parser\\UnknownNamespaceException' => $baseDir . '/Core/Fluid/src/Core/Parser/UnknownNamespaceException.php',
    'TYPO3Fluid\\Fluid\\Core\\Rendering\\RenderingContext' => $baseDir . '/Core/Fluid/src/Core/Rendering/RenderingContext.php',
    'TYPO3Fluid\\Fluid\\Core\\Rendering\\RenderingContextInterface' => $baseDir . '/Core/Fluid/src/Core/Rendering/RenderingContextInterface.php',
    'TYPO3Fluid\\Fluid\\Core\\Variables\\ChainedVariableProvider' => $baseDir . '/Core/Fluid/src/Core/Variables/ChainedVariableProvider.php',
    'TYPO3Fluid\\Fluid\\Core\\Variables\\JSONVariableProvider' => $baseDir . '/Core/Fluid/src/Core/Variables/JSONVariableProvider.php',
    'TYPO3Fluid\\Fluid\\Core\\Variables\\StandardVariableProvider' => $baseDir . '/Core/Fluid/src/Core/Variables/StandardVariableProvider.php',
    'TYPO3Fluid\\Fluid\\Core\\Variables\\VariableExtractor' => $baseDir . '/Core/Fluid/src/Core/Variables/VariableExtractor.php',
    'TYPO3Fluid\\Fluid\\Core\\Variables\\VariableProviderInterface' => $baseDir . '/Core/Fluid/src/Core/Variables/VariableProviderInterface.php',
    'TYPO3Fluid\\Fluid\\Core\\ViewHelper\\AbstractConditionViewHelper' => $baseDir . '/Core/Fluid/src/Core/ViewHelper/AbstractConditionViewHelper.php',
    'TYPO3Fluid\\Fluid\\Core\\ViewHelper\\AbstractTagBasedViewHelper' => $baseDir . '/Core/Fluid/src/Core/ViewHelper/AbstractTagBasedViewHelper.php',
    'TYPO3Fluid\\Fluid\\Core\\ViewHelper\\AbstractViewHelper' => $baseDir . '/Core/Fluid/src/Core/ViewHelper/AbstractViewHelper.php',
    'TYPO3Fluid\\Fluid\\Core\\ViewHelper\\ArgumentDefinition' => $baseDir . '/Core/Fluid/src/Core/ViewHelper/ArgumentDefinition.php',
    'TYPO3Fluid\\Fluid\\Core\\ViewHelper\\Exception' => $baseDir . '/Core/Fluid/src/Core/ViewHelper/Exception.php',
    'TYPO3Fluid\\Fluid\\Core\\ViewHelper\\TagBuilder' => $baseDir . '/Core/Fluid/src/Core/ViewHelper/TagBuilder.php',
    'TYPO3Fluid\\Fluid\\Core\\ViewHelper\\Traits\\CompileWithContentArgumentAndRenderStatic' => $baseDir . '/Core/Fluid/src/Core/ViewHelper/Traits/CompileWithContentArgumentAndRenderStatic.php',
    'TYPO3Fluid\\Fluid\\Core\\ViewHelper\\Traits\\CompileWithRenderStatic' => $baseDir . '/Core/Fluid/src/Core/ViewHelper/Traits/CompileWithRenderStatic.php',
    'TYPO3Fluid\\Fluid\\Core\\ViewHelper\\ViewHelperInterface' => $baseDir . '/Core/Fluid/src/Core/ViewHelper/ViewHelperInterface.php',
    'TYPO3Fluid\\Fluid\\Core\\ViewHelper\\ViewHelperInvoker' => $baseDir . '/Core/Fluid/src/Core/ViewHelper/ViewHelperInvoker.php',
    'TYPO3Fluid\\Fluid\\Core\\ViewHelper\\ViewHelperResolver' => $baseDir . '/Core/Fluid/src/Core/ViewHelper/ViewHelperResolver.php',
    'TYPO3Fluid\\Fluid\\Core\\ViewHelper\\ViewHelperVariableContainer' => $baseDir . '/Core/Fluid/src/Core/ViewHelper/ViewHelperVariableContainer.php',
    'TYPO3Fluid\\Fluid\\Exception' => $baseDir . '/Core/Fluid/src/Exception.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\AliasViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/AliasViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\Cache\\DisableViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/Cache/DisableViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\Cache\\StaticViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/Cache/StaticViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\Cache\\WarmupViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/Cache/WarmupViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\CaseViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/CaseViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\CommentViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/CommentViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\CountViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/CountViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\CycleViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/CycleViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\DebugViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/DebugViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\DefaultCaseViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/DefaultCaseViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\ElseViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/ElseViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\ForViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/ForViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\Format\\CdataViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/Format/CdataViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\Format\\HtmlspecialcharsViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/Format/HtmlspecialcharsViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\Format\\PrintfViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/Format/PrintfViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\Format\\RawViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/Format/RawViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\GroupedForViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/GroupedForViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\IfViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/IfViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\LayoutViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/LayoutViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\OrViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/OrViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\RenderViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/RenderViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\SectionViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/SectionViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\SpacelessViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/SpacelessViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\SwitchViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/SwitchViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\ThenViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/ThenViewHelper.php',
    'TYPO3Fluid\\Fluid\\ViewHelpers\\VariableViewHelper' => $baseDir . '/Core/Fluid/src/ViewHelpers/VariableViewHelper.php',
    'TYPO3Fluid\\Fluid\\View\\AbstractTemplateView' => $baseDir . '/Core/Fluid/src/View/AbstractTemplateView.php',
    'TYPO3Fluid\\Fluid\\View\\AbstractView' => $baseDir . '/Core/Fluid/src/View/AbstractView.php',
    'TYPO3Fluid\\Fluid\\View\\Exception' => $baseDir . '/Core/Fluid/src/View/Exception.php',
    'TYPO3Fluid\\Fluid\\View\\Exception\\InvalidSectionException' => $baseDir . '/Core/Fluid/src/View/Exception/InvalidSectionException.php',
    'TYPO3Fluid\\Fluid\\View\\Exception\\InvalidTemplateResourceException' => $baseDir . '/Core/Fluid/src/View/Exception/InvalidTemplateResourceException.php',
    'TYPO3Fluid\\Fluid\\View\\TemplatePaths' => $baseDir . '/Core/Fluid/src/View/TemplatePaths.php',
    'TYPO3Fluid\\Fluid\\View\\TemplateView' => $baseDir . '/Core/Fluid/src/View/TemplateView.php',
    'TYPO3Fluid\\Fluid\\View\\ViewInterface' => $baseDir . '/Core/Fluid/src/View/ViewInterface.php',
);
