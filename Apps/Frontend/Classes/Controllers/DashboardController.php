<?php
namespace Apps\Frontend\Controllers;

class DashboardController extends \Framework\Core\Frontend\AbstractController{

	public function injectRepositories(){
		$this->dashboardRepository = $this->injectRepository("Apps\\Frontend\\Repository\\DashboardRepository");
	}

	public function indexAction(){
		// \Kint::dump($this);
		$this->view->assign('name', 'Manthan');
		$this->render();
	}

}

?>